# Asus UX302 kernel project

This kernel build script is made for UX302 laptops using kernels from kernel.org. The script has only been tested with Arch Linux, but could easily be altered for use with other distributions for those with some basic Linux knowledge. Each time a new release candidate or stable version comes out, variables in the script need to tweaked to faciliate the version. 

Why use this kernel?
The popular distribution and mainline kernels all had problems with this laptop in the beginning.  While many of the issues have been fixed in these kernels, they still don't have a working MMC/SDCard driver. The script uses a custom kernel configuration file so that it removes MANY unnecessary modules and kernel features from the build. The kernel configuration was modified for this hardware specifically. The script applies some minor tweaks using "sed" during the build process (see the script source for details).

------------------------------
Milestones:

2014-02-26 - initial commit using 3.14 rc 4. Fix blank screen issue using ignore_bpp_clamping quirk. ASUS UX302LA/LG incorrectly returns 18bpp in UEFI mode from vbe edp data.
The UX302 models have an issue with resume after suspend. The screen stays dark, but the laptop is still running. It is hard to tell how long my 3.14 patch for the suspend issue will be good for as the kernel source keeps changing frequently in this area. The suspend patch I apply is simply a tweaked version of the original patch from Alberto Aguirre: https://launchpadlibrarian.net/166248804/0001-drm-i915-Add-quirk-for-ASUS-UX302LA-G-to-fix-blank-s.patch

2014-04-27 - after upgrading and testing 3.15-rc2, the resume/suspend blank screen patch is no longer needed.

2014-04-29 - added Alcor Micro AU6601 SD controller driver (thanks Oleksij Rempel) 
	
2014-06-10 - 3.15.0 stable released. Includes fully functional MMC SDCard reader (thanks Oleksij Rempel for the updates).

2014-09-11 - 3.16.1 stable released. Same kernel features, now on stable 3.16.1.

------------------------------
Instructions:

The first time you run the script, I recommend opening the script in a text editor and then executing one command at a time in your terminal so you can monitor/learn what it does and fix build dependency issues if they arise.  Just make sure you use sudo or equivalent for the removal/installation commands near the end since they will require root permission. 

Make sure you keep your working kernel in place as a backup in case their are issues with your build.

1) Download the version (i.e. commit version) of the script you want to ruse. Filename = script "motley_asus_ux302_custom_kernel.sh" (see git history for other versions/builds). I recommend the current version if you are unsure.

	-Note: each commit version of the script references the defconfig and patch file URLs used in the THAT commit version. Be aware as you make changes.

2) Make it executable

  $ chmod a+x motley_asus_ux302_custom_kernel.sh

3) Run the script with root permission!! This is needed for it to install properly (it will prompt for password if you use sudo or equivalent)

  $ sudo ./motley_asus_ux302_custom_kernel.sh

4) Create a grub entry (or an entry in another bootloader of your choice)

Note: this step only needs to be done once. In other words, the GRUB entry is not kernel version specific, so if you have to run the kernel script again, it will continue to use the same GRUB entry.

Here is my GRUB menu entry as an example. I still use "acpi_osi=" to allow the keyboard dimmer function keys to work.

menuentry 'Mainline motley' --class antergos --class gnu-linux --class gnu --class os $menuentry_id_option 'gnulinux--mainline-48c5f43e-905d-455d-b16a-d8fff53cbef4' {
	load_video
	set gfxpayload=keep
	insmod gzio
	insmod part_msdos 
	insmod ext2
	set root='hd1,msdos1'
	if [ x$feature_platform_search_hint = xy ]; then
	  search --no-floppy --fs-uuid --set=root --hint-bios=hd1,msdos1 --hint-efi=hd1,msdos1 --hint-baremetal=ahci1,msdos1  48c5f43e-905d-455d-b16a-d8fff53cbef4
	else
	  search --no-floppy --fs-uuid --set=root 48c5f43e-905d-455d-b16a-d8fff53cbef4
	fi
	echo	'Loading Linux  ...'
	linux	/boot/vmlinuz-mainline-motley root=UUID=48c5f43e-905d-455d-b16a-d8fff53cbef4 rw  quiet acpi_osi=
	echo	'Loading initial ramdisk ...'
	initrd	/boot/initramfs-mainline-motley.img
}




